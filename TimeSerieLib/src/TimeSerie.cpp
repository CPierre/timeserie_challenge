#include "TimeSerie.h"

TimeSerie::TimeSerie()
{

}

/*
 * Adds a new pair (time, value) to the time serie.
 */
void TimeSerie::addTimeSerieValue(double time, double value)
{
    std::pair<std::map<double, double>::iterator, bool> insertValue;
    insertValue = timeSerieData.insert(std::pair<double, double>(time, value));
    if (insertValue.second == false) {
        std::cout << "Already existing value with time = " << time << std::endl;
    }
}

bool TimeSerie::isEmpty()
{
    return timeSerieData.empty();
}

int TimeSerie::count()
{
    return (int)timeSerieData.size();
}

/*
 * Returns the closest value for the given time. Returns the exact value if it exists.
 */
double TimeSerie::getClosestValueAtTime(double time)
{
    if (isEmpty()) {
        // No data
        return -DBL_MAX;
    }

    std::map<double, double>::iterator previousValue, lowerBoundValue;
    lowerBoundValue = timeSerieData.lower_bound(time);

    if (lowerBoundValue == timeSerieData.begin()) {
        // Asked data is before all data
        return lowerBoundValue->second;
    }

    previousValue = lowerBoundValue;
    previousValue--;

    if (lowerBoundValue == timeSerieData.end()) {
        // Asked data is after all data
        return previousValue->second;
    }

    double intervalCenter = previousValue->first + (lowerBoundValue->first - previousValue->first) / 2;
    if (time < intervalCenter) {
        return previousValue->second;
    } else {
        return lowerBoundValue->second;
    }
}

/*
 * Returns an interpolated value for the given time, computed with the bounding values.
 * Returns the exact value if it exists.
 */
double TimeSerie::getInterpolatedValueAtTime(double time)
{
    if (isEmpty()) {
        // No data
        return -DBL_MAX;
    }

    std::map<double, double>::iterator previousValue, lowerBoundValue;
    lowerBoundValue = timeSerieData.lower_bound(time);

    if (lowerBoundValue == timeSerieData.begin()) {
        // Asked data is before all data
        return lowerBoundValue->second;
    }

    previousValue = lowerBoundValue;
    previousValue--;

    if (lowerBoundValue == timeSerieData.end()) {
        // Asked data is after all data
        return previousValue->second;
    }

    if (lowerBoundValue->first == time) {
        // Asked data exists, return it
        return lowerBoundValue->second;
    }

    double intervalSize = lowerBoundValue->first - previousValue->first;
    double intervalToPrevious = time - previousValue->first;
    double intervalToNext = lowerBoundValue->first - time;

    double interpolatedValue = (intervalToNext / intervalSize) * previousValue->second + (intervalToPrevious / intervalSize) * lowerBoundValue->second;
    return interpolatedValue;
}

std::map<double, double>::const_iterator TimeSerie::getBeginIterator()
{
    return timeSerieData.cbegin();
}

std::map<double, double>::const_iterator TimeSerie::getEndIterator()
{
    return timeSerieData.cend();
}

void TimeSerie::printTimeSerie()
{
    for (std::map<double, double>::iterator it = timeSerieData.begin(); it != timeSerieData.end(); ++it) {
        std::cout << it->first << " - " << it->second << "\n";
    }
}
