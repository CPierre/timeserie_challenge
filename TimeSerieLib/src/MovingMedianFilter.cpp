#include "MovingMedianFilter.h"

MovingMedianFilter::MovingMedianFilter(int period) : Filter(period)
{

}

TimeSerie *MovingMedianFilter::filter(TimeSerie &serie)
{
    TimeSerie * filtered = computeMovingMedian(serie, m_period);

    return filtered;
}

/*
 * For every values of the time serie, computes the median of a "period" sized window
 * before the value. Therefore first "period" values have no moving median.
 */
TimeSerie *MovingMedianFilter::computeMovingMedian(TimeSerie &serie, int period)
{
    TimeSerie * movingMedian = new TimeSerie();

    if (period < 1) {
        return movingMedian;
    }

    if (period > serie.count()) {
        // Not enough data in time serie for the given period
        return movingMedian;
    }

    // This subserie represents the moving window
    std::list<double> subSerie;

    std::map<double, double>::const_iterator it;
    double median = 0;
    int count = 0;
    for (it = serie.getBeginIterator(); it != serie.getEndIterator(); ++it) {
        subSerie.push_back(it->second);
        if (count < period - 1) {
            // We don't have enough values yet
            count++;
        } else if (count == period - 1) {
            // Now we do
            median = computeMedianOf(subSerie);
            movingMedian->addTimeSerieValue(it->first, median);
            count++;
        } else {
            // Remove the "period + 1" value from the subserie, moving the window forward
            subSerie.pop_front();
            median = computeMedianOf(subSerie);
            movingMedian->addTimeSerieValue(it->first, median);
        }
    }

    return movingMedian;
}

/*
 * Computes the median of a list of double. If the list is odd, returns
 * the centered value. If the list is even, returns a linear interpolation
 * between the 2 center values.
 */
double MovingMedianFilter::computeMedianOf(std::list<double> &list)
{
    if (list.empty()) {
        return -DBL_MAX;
    }

    // Since we will sort the list, we need a copy of it
    std::list<double> copy = list;

    double median = 0;

    copy.sort();
    std::list<double>::const_iterator it = copy.begin();

    if (copy.size() % 2 == 0) {
        int middleIndex = (int)copy.size() / 2;

        for (int i = 0; i < middleIndex - 1; i++) {
            it++;
        }

        double lowerValue = *it;
        it++;
        double upperValue = *it;
        median = (lowerValue + upperValue) / 2.f;
    } else {
        int middleIndex = (int)ceil(copy.size() / 2.f) - 1;

        for (int i = 0; i < middleIndex; i++) {
            it++;
        }

        median = *it;
    }

    return median;
}
