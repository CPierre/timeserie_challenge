#include "TimeSerieAnalyser.h"

double TimeSerieAnalyser::computeAverageOf(TimeSerie &serie)
{
    if (serie.isEmpty()) {
        std::cout << "TimeSerie is empty !" << std::endl;
        return 0;
    }

    double sum = 0;
    int count = 0;
    for (std::map<double, double>::const_iterator it = serie.getBeginIterator(); it != serie.getEndIterator(); ++it) {
        sum += it->second;
        count++;
    }

    return sum / count;
}

double TimeSerieAnalyser::computeVarianceOf(TimeSerie &serie)
{
    if (serie.isEmpty()) {
        std::cout << "TimeSerie is empty !" << std::endl;
        return 0;
    }

    double average = computeAverageOf(serie);
    double sum = 0;
    int count = 0;
    for (std::map<double, double>::const_iterator it = serie.getBeginIterator(); it != serie.getEndIterator(); ++it) {
        sum += pow(it->second - average, 2);
        count++;
    }

    return sum / count;
}

/*
 * Computes the correlation between 2 time series. Since those series don't necessarily have values at the same time,
 * we interpolate the missing values.
 *
 *  serie1  |-V---I----I---V-I-V------V-----I-I----V---I-|
 *  serie2  |-I---V----V---I-V-I------I-----V-V----I---V-|
 *  time    |-|---|----|---|-|-|------|-----|-|----|---|-|
 *
 *  V = existing value
 *  I = interpolated value
 *
 */
double TimeSerieAnalyser::computeCorrelationBetween(TimeSerie &serie1, TimeSerie &serie2)
{
    if (serie1.isEmpty() || serie2.isEmpty()) {
        std::cout << "One or both of the TimeSerie are empty !" << std::endl;
        return -FLT_MAX;
    }

    std::map<double, double>::const_iterator serie1It, serie2It;
    serie1It = serie1.getBeginIterator();
    serie2It = serie2.getBeginIterator();

    double serie1Average = computeAverageOf(serie1);
    double serie2Average = computeAverageOf(serie2);
    double serie1Variance = computeVarianceOf(serie1);
    double serie2Variance = computeVarianceOf(serie2);

    double correlation = 0;
    double covariance = 0;

    double sum = 0;
    int count = 0;
    while (serie1It != serie1.getEndIterator() && serie2It != serie2.getEndIterator()) {
        double time = 0;
        if (serie1It != serie1.getEndIterator() && serie1It->first <= serie2It->first) {
            // We take the value of serie1, and interpolate one for serie2 at the same time
            time = serie1It->first;
            serie1It++;
        } else if (serie2It != serie2.getEndIterator()) {
            // We take the value of serie2, and interpolate one for serie1 at the same time
            time = serie2It->first;
            serie2It++;
        }
        sum += (serie1.getInterpolatedValueAtTime(time) - serie1Average) * (serie2.getInterpolatedValueAtTime(time) - serie2Average);
        count++;
    }

    covariance = sum / count;
    correlation = covariance / (sqrt(serie1Variance) * sqrt(serie2Variance));
    return correlation;
}

/*
 * Returns a new TimeSerie created by applying the Filter to the TimeSerie.
 * Filter is an abstract class.
 */
TimeSerie *TimeSerieAnalyser::applyFilterTo(TimeSerie &serie, Filter &filter)
{
    TimeSerie * filtered = filter.filter(serie);

    return filtered;
}
