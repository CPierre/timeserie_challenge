#include "MovingAverageFilter.h"

MovingAverageFilter::MovingAverageFilter(int period) : Filter(period)
{

}

TimeSerie *MovingAverageFilter::filter(TimeSerie &serie)
{
    TimeSerie * filtered = computeMovingAverage(serie, m_period);

    return filtered;
}

/*
 * For every values of the time serie, computes the average of a "period" sized window
 * before the value. Therefore first "period" values have no moving average.
 */
TimeSerie *MovingAverageFilter::computeMovingAverage(TimeSerie &serie, int period)
{
    TimeSerie * movingAverage = new TimeSerie();

    if (period < 1) {
        return movingAverage;
    }

    if (period > serie.count()) {
        // Not enough data in time serie for the given period
        return movingAverage;
    }

    std::map<double, double>::const_iterator it, substractIt;
    substractIt = serie.getBeginIterator();
    double sum = 0;
    int count = 0;
    for (it = serie.getBeginIterator(); it != serie.getEndIterator(); ++it) {
        sum += it->second;
        if (count < period - 1) {
            // We don't have enough values yet
            count++;
        } else if (count == period - 1) {
            // Now we do
            movingAverage->addTimeSerieValue(it->first, sum / period);
            count++;
        } else {
            // Substract the "period + 1" value from the sum, moving the window forward
            sum -= substractIt->second;
            movingAverage->addTimeSerieValue(it->first, sum / period);
            substractIt++;
        }
    }

    return movingAverage;
}
