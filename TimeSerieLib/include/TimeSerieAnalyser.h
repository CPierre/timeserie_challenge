#ifndef TIMESERIEANALYSER_H
#define TIMESERIEANALYSER_H

#include <iostream>
#include <math.h>

#include "TimeSerie.h"
#include "Filter.h"

class TimeSerieAnalyser {

public:
    double computeAverageOf(TimeSerie &serie);
    double computeVarianceOf(TimeSerie &serie);
    double computeCorrelationBetween(TimeSerie &serie1, TimeSerie &serie2);

    TimeSerie * applyFilterTo(TimeSerie &serie, Filter &filter);

private:
};

#endif
