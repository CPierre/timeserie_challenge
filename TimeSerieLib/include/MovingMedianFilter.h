#ifndef MOVINGMEDIANFILTER_H
#define MOVINGMEDIANFILTER_H

#include <list>

#include "Filter.h"

class MovingMedianFilter : public Filter {
public:
    MovingMedianFilter(int period);
    virtual TimeSerie * filter(TimeSerie &serie);
private:
    TimeSerie * computeMovingMedian(TimeSerie &serie, int period);
    double computeMedianOf(std::list<double> &list);
};

#endif
