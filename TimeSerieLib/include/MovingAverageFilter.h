#ifndef MOVINGAVERAGEFILTER_H
#define MOVINGAVERAGEFILTER_H

#include "Filter.h"

class MovingAverageFilter : public Filter {

public:
    MovingAverageFilter(int period);
    virtual TimeSerie * filter(TimeSerie &serie);

private:
    TimeSerie * computeMovingAverage(TimeSerie &serie, int period);
};

#endif
