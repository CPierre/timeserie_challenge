#ifndef FILTER_H
#define FILTER_H

#include "TimeSerie.h"

/*
 * Abstract class for filters for time series. Those filters all have at least a period as parameter,
 * but more parameters can be added to children classes.
 */

class Filter {

public:
    Filter(int period);
    virtual TimeSerie * filter(TimeSerie &serie) = 0;

protected:
    int m_period;
};

#endif
