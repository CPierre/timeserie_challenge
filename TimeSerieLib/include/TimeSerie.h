#ifndef TIMESERIE_H
#define TIMESERIE_H

#include <iostream>
#include <map>

class TimeSerie {

public:
    TimeSerie();

    // Data access
    double getClosestValueAtTime(double time);
    double getInterpolatedValueAtTime(double time);
    std::map<double, double>::const_iterator getBeginIterator();
    std::map<double, double>::const_iterator getEndIterator();

    // Data modification
    void addTimeSerieValue(double time, double value);

    // Utility
    bool isEmpty();
    int count();
    void printTimeSerie();

private:
    std::map<double, double> timeSerieData;
};

#endif
