#include <iostream>

#include "TimeSerie.h"
#include "TimeSerieAnalyser.h"
#include "MovingAverageFilter.h"
#include "MovingMedianFilter.h"

int main (int argc, char * argv[]) {

    TimeSerie serie1, serie2, unorderedSerie;
    TimeSerieAnalyser analyser;

    // Add values to the first serie (time, value)

    serie1.addTimeSerieValue(12, 500);
    serie1.addTimeSerieValue(17.5, 350);
    serie1.addTimeSerieValue(19, 250);
    serie1.addTimeSerieValue(28, 730);
    serie1.addTimeSerieValue(31.2, 960);

    std::cout << "Serie1 values :" << std::endl << std::endl;

    serie1.printTimeSerie();

    std::cout << std::endl;

    // Retrieve closest value for a given time

    std::cout << "Closest values :" << std::endl << std::endl;

    std::cout << "time = 17.5 : " << serie1.getClosestValueAtTime(17.5) << std::endl;
    std::cout << "time = 8 : " << serie1.getClosestValueAtTime(8) << std::endl;
    std::cout << "time = 45 : " << serie1.getClosestValueAtTime(45) << std::endl;
    std::cout << "time = 23.4 : " << serie1.getClosestValueAtTime(23.4) << std::endl;
    std::cout << "time = 23.5 : " << serie1.getClosestValueAtTime(23.5) << std::endl;
    std::cout << "time = 23.6 : " << serie1.getClosestValueAtTime(23.6) << std::endl;

    std::cout << std::endl;

    // Retrieve interpolated value for a given time

    std::cout << "Interpolated values :" << std::endl << std::endl;

    std::cout << "time = 17.5 : " << serie1.getInterpolatedValueAtTime(17.5) << std::endl;
    std::cout << "time = 8 : " << serie1.getInterpolatedValueAtTime(8) << std::endl;
    std::cout << "time = 45 : " << serie1.getInterpolatedValueAtTime(45) << std::endl;
    std::cout << "time = 23.4 : " << serie1.getInterpolatedValueAtTime(23.4) << std::endl;
    std::cout << "time = 23.5 : " << serie1.getInterpolatedValueAtTime(23.5) << std::endl;
    std::cout << "time = 23.6 : " << serie1.getInterpolatedValueAtTime(23.6) << std::endl;

    std::cout << std::endl;

    std::cout << "Serie1 Average = " << analyser.computeAverageOf(serie1) << std::endl;

    std::cout << "Serie1 Variance = " << analyser.computeVarianceOf(serie1) << std::endl << std::endl;

    // Add values to the second serie (time, value)

    serie2.addTimeSerieValue(8, 260);
    serie2.addTimeSerieValue(13.8, 15);
    serie2.addTimeSerieValue(18, 125.3);
    serie2.addTimeSerieValue(25, 333);
    serie2.addTimeSerieValue(29.9, 467);
    serie2.addTimeSerieValue(35, 350);

    std::cout << "Serie2 values :" << std::endl << std::endl;

    serie2.printTimeSerie();

    std::cout << std::endl;

    std::cout << "Serie2 Average = " << analyser.computeAverageOf(serie2) << std::endl;

    std::cout << "Serie2 Variance = " << analyser.computeVarianceOf(serie2) << std::endl << std::endl;

    std::cout << "Correlation between serie1 and serie2 = " << analyser.computeCorrelationBetween(serie1, serie2) << std::endl << std::endl;

    // Compute moving average of serie1 as a new time serie, with period = 3

    std::cout << "Moving average of serie1 with period = 3 :" << std::endl << std::endl;
    TimeSerie * serie1MovingAverage = analyser.applyFilterTo(serie1, MovingAverageFilter(3));
    serie1MovingAverage->printTimeSerie();

    std::cout << std::endl;

    // Compute moving median of serie2 as a new time serie, with period = 2

    std::cout << "Moving median of serie2 with period = 2 :" << std::endl << std::endl;
    TimeSerie * serie2MovingMedian = analyser.applyFilterTo(serie2, MovingMedianFilter(2));
    serie2MovingMedian->printTimeSerie();

    std::cout << std::endl;

    // Add unordered values to the serie

    unorderedSerie.addTimeSerieValue(1, 100);
    unorderedSerie.addTimeSerieValue(5, 500);
    unorderedSerie.addTimeSerieValue(3, 300);
    unorderedSerie.addTimeSerieValue(4, 400);
    unorderedSerie.addTimeSerieValue(2, 200);

    std::cout << "Unordered serie values :" << std::endl << std::endl;

    unorderedSerie.printTimeSerie();

    delete serie1MovingAverage, serie2MovingMedian;
}
